
# Documentation and Software Licences for this Repository

Unless otherwise stated the documentation in this repository is licensed under
Creative Commons BY-NC-ND. This license allows reusers to copy and distribute
the material in any medium or format in unadapted form only, for noncommercial
purposes only, and only so long as attribution is given to the creator. For
details see
[creativecommons.org/licenses/by-nc-sa/4.0/](https://creativecommons.org/licenses/by-nc-sa/4.0/).

Unless otherwise stated the code in this repository is licensed under GNU GPL
3. This license is intended to guarantee your freedom to share and change all
versions of a program, and to make sure it remains free software for all its
users.  For details see
[gnu.org/licenses/gpl-3.0.en.html](https://www.gnu.org/licenses/gpl-3.0.en.html).

